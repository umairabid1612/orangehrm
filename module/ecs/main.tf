resource "aws_ecs_cluster" "fargate_cluster" {
  name = "my-fargate-cluster"
}

resource "aws_ecs_task_definition" "my_task_definition" {
  family                   = "my-task-family"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  container_definitions    = jsonencode([{
    name      = "my-container"
    image     = "public.ecr.aws/l0o9a2r1/orangehrm:5cd056d1"
    cpu       = 256   # CPU units for the container
    memory    = 512   # Memory in MiB for the container
    port_mappings = [{
      container_port = 80
      host_port      = 80
      protocol       = "tcp"
    }]
  }])
}

resource "aws_ecs_service" "my_service" {
  name            = "my-ecs-service"
  cluster         = aws_ecs_cluster.fargate_cluster.id
  task_definition = aws_ecs_task_definition.my_task_definition.arn
  desired_count   = 1  # Number of tasks to run
  launch_type     = "FARGATE"
  
  network_configuration {
    # Use the default subnets for the default VPC
    subnets = var.public_subnets
    security_groups = [var.sg_id]
    assign_public_ip = true  # Set to false if your subnets are private
  }
}


