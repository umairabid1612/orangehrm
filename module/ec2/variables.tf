variable "public_subnet_id" {
  type        = string
  description = "ID of the public subnet"
}

variable "private_db_subnet_id" {
  type        = string
  description = "ID of the private database subnet"
}

variable "key_pair_name" {
  type        = string
  description = "Name of the key pair"
}

variable "jump_server_ami" {
  type        = string
  description = "AMI ID for the jump server instance"
}

variable "private_db_ami" {
  type        = string
  description = "AMI ID for the private database instance"
}

variable "jump_server_sg" {
  type = string
}

variable "private_db_sg" {
  type = string
}