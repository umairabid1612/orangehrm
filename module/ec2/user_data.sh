#!/bin/bash
yum update -y
yum install mariadb-server git -y

# Modify MySQL configuration
echo "bind-address=0.0.0.0" | sudo tee -a /etc/my.cnf

systemctl start mariadb
systemctl enable mariadb
sudo mysqladmin password 'root@1234'

# Create the MySQL configuration file for the root user
echo "[client]" | sudo tee mysql_config_root.cnf
echo "user=root" | sudo tee -a mysql_config_root.cnf
echo "password=root@1234" | sudo tee -a mysql_config_root.cnf

# Log in to MySQL as root and perform the required database operations
sudo mysql --defaults-extra-file=mysql_config_root.cnf << EOF
-- Create a user for the Flask application and grant privileges
CREATE USER 'orangehrm'@'localhost' IDENTIFIED BY 'orange1234';
CREATE USER 'orangehrm'@'%' IDENTIFIED BY 'orange1234';

GRANT ALL PRIVILEGES ON *.* TO 'orangehrm'@'%' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'orangehrm'@'localhost' WITH GRANT OPTION;

-- Optionally, create a new database for the Flask application
CREATE DATABASE orange_db;

-- Flush privileges to apply the changes
FLUSH PRIVILEGES;
EOF
