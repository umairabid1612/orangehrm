output "sg_id" {
    value = aws_security_group.ecs_cluster_sg.id
}

output "jump_server_sg_id" {
  value = aws_security_group.jump_server_sg.id
}
output "private_db_sg_id" {
  value = aws_security_group.private_db_sg.id
}
output "private_db_sg" {
  value = aws_security_group.private_db_sg.name
}

output "jump_server_sg" {
  value = aws_security_group.jump_server_sg.name
}