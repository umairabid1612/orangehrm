terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.10.0"
    }
  }
  backend "s3" {
    bucket  = "terraform-state-bucket-musk"
    key     = "terraform/state/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}

provider "aws" {
  # Configuration options
  region = "us-east-1"
}

module "vpc" {
    source = "./module/vpc"
    vpc_cidr_block = "10.0.0.0/16"
    public_subnet_cidrs = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}
module "security_group" {
    source = "./module/security-group"
    vpc_id = module.vpc.vpc_id
}

module "ecs" {
    source = "./module/ecs"
    sg_id = module.security_group.sg_id
    public_subnets = module.vpc.public_subnet_ids
}

module "ec2" {
  source = "./module/ec2"
  public_subnet_id = module.vpc.public_subnet_ids[0]
  private_db_subnet_id = module.vpc.public_subnet_ids[1]
  key_pair_name = "html-app-kp"
  jump_server_ami = "ami-09538990a0c4fe9be"
  private_db_ami = "ami-09538990a0c4fe9be"
  jump_server_sg = module.security_group.jump_server_sg_id
  private_db_sg = module.security_group.private_db_sg_id
}