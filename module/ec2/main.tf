resource "aws_instance" "jump_server_instance" {
  ami                    = var.jump_server_ami
  instance_type          = "t2.micro"
  subnet_id              = var.public_subnet_id
  key_name               = var.key_pair_name
  associate_public_ip_address = true
  security_groups = [var.jump_server_sg]
  tags = {
    Name = "jump-server"
  }
}

resource "aws_instance" "private_db_instance" {
  ami                    = var.private_db_ami
  instance_type          = "t2.micro"
  subnet_id              = var.private_db_subnet_id
  key_name               = var.key_pair_name
  user_data              = filebase64("${path.module}/user_data.sh")
  associate_public_ip_address = true
  security_groups = [var.private_db_sg]
  tags = {
    Name = "db-server"
  }
}


