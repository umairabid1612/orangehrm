resource "aws_vpc" "node-app-vpc" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "public_subnets" {
  count             = length(var.public_subnet_cidrs)
  vpc_id            = aws_vpc.node-app-vpc.id
  cidr_block        = var.public_subnet_cidrs[count.index]
  availability_zone = element(["us-east-1a", "us-east-1b", "us-east-1c"], count.index)
  tags = {
    Name = "public-subnet-${count.index + 1}"
  }
}


resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.node-app-vpc.id
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.node-app-vpc.id
}

resource "aws_route" "public_route" {
  route_table_id         = aws_route_table.public_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.my_igw.id
}

resource "aws_route_table_association" "public_subnet_associations" {
  count          = length(aws_subnet.public_subnets)
  subnet_id      = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}

