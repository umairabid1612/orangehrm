variable "vpc_cidr_block" {
  type        = string
  description = "ID of the VPC"
}

variable "public_subnet_cidrs" {
  type = list(string)
}