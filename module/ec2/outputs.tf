output "jump_server_public_ip" {
  value = aws_instance.jump_server_instance.public_ip
}

output "private_db_private_ip" {
  value = aws_instance.private_db_instance.private_ip
}
